from streamlit.connections import ExperimentalBaseConnection
import sqlite3
import pandas as pd
import streamlit as st


class SQLiteConnection(ExperimentalBaseConnection[sqlite3.Connection]):
    def __init__(self, url: str, **kwargs):
        self.url = url

    def _connect(self) -> sqlite3.Connection:
        connection = sqlite3.connect(self.url)
        return connection.cursor()
    

    @st.cache_data(ttl=3600,experimental_allow_widgets=True)
    def query(_self, query) -> pd.DataFrame:
        cursor = _self._connect()
        cursor.execute(query)
        return pd.DataFrame(cursor.fetchall(), columns=[x[0] for x in cursor.description])
        
        
    
    