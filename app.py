import streamlit as st
from sqlite_connect import SQLiteConnection

page = st.sidebar.radio("Select your page", ['Intro','All', 'Books', 'Authors', 'Range'])
conn = st.experimental_connection('google_books.db',type=SQLiteConnection,url = "google_books.db")

if page == 'Intro':
    st.markdown(""" 
# Welcome to the Digital Library!

This website allows you to search for books by title, author, or genre. You can also view detailed information about a book, including its rating, number of voters, price, page count, publisher, and published date.

To get started, select the page you want to view from the sidebar. The main content area will then display the corresponding information.

## Pages

The Digital Library has four pages:

* **All:** This page displays a list of all the books in the library. Users can sort the list by title, author, rating, or price.

* **Books:** This page allows users to search for books by title, author, or genre. Once a book is selected, the user can view detailed information about the book, including its rating, number of voters, price, page count, publisher, and published date.

* **Authors:** This page displays a list of all the authors in the library. Users can sort the list by name or number of books. Once an author is selected, the user can view a list of all the books by that author.

* **Range:** This page allows users to search for books by rating, price, and page count. Users can use the sliders to specify the range of values they want to search for. Once the search is complete, the user will see a list of books that meet the criteria.

## Connector Hackathon Submission
                
### SQLiteConnection Class

This code defines a `SQLiteConnection` class that can be used to connect to a SQLite database. The class inherits from the `ExperimentalBaseConnection` class, which provides a way to connect to a data source in Streamlit.

The `SQLiteConnection` class has a constructor that takes the URL of the SQLite database as a parameter. The class also has a `_connect()` method that returns a cursor object that can be used to execute queries against the database.

The `SQLiteConnection` class also has a `query()` method that takes a query as a parameter and returns a Pandas DataFrame with the results of the query. The `query()` method uses the `st.cache_data()` decorator to cache the results of the query for 3600 seconds.

## How to use the code

To use the `SQLiteConnection` class, you first need to create an instance of the class. You can do this by passing the URL of the SQLite database to the constructor. For example:

```python
conn = st.experimental_connection('db',type=SQLiteConnection,url = "db location")


Once you have created an instance of the class, you can use the `_connect()` method to get a cursor object. You can then use the cursor object to execute queries against the database. For example:

python
cursor = connection._connect()
cursor.execute('SELECT * FROM books')
results = cursor.fetchall()
```

We hope you enjoy using our website!
""")

if page == 'All':
    st.title("Welcome to digital library!")
    books = conn.query('SELECT * FROM books')
    st.dataframe(books)

if page == 'Books':
    book_names = (conn.query('SELECT title FROM books'))
    st.write("select the name of the book")
    book_name = st.selectbox('Books',book_names)

    book = conn.query(f"SELECT title FROM books WHERE title = '{book_name}' limit 1;").values[0][0]
    desc = conn.query(f"SELECT description FROM books WHERE title = '{book_name}' limit 1;").values[0][0]
    author = conn.query(f"SELECT author FROM books WHERE title = '{book_name}' limit 1;").values[0][0]
    rating = conn.query(f"SELECT rating FROM books WHERE title = '{book_name}' limit 1;").values[0][0]
    voters = conn.query(f"SELECT voters FROM books WHERE title = '{book_name}' limit 1;").values[0][0]
    price = conn.query(f"SELECT price FROM books WHERE title = '{book_name}' limit 1;").values[0][0]
    publisher = conn.query(f"SELECT publisher FROM books WHERE title = '{book_name}' limit 1;").values[0][0]
    page_count  = conn.query(f"SELECT page_count FROM books WHERE title = '{book_name}' limit 1;").values[0][0]
    year = conn.query(f"SELECT published_date FROM books WHERE title = '{book_name}' limit 1;").values[0][0]
    genre = conn.query(f"SELECT genres FROM books WHERE title = '{book_name}' limit 1;").values[0][0]

    col1, col2, col3, col4 = st.columns(4)
    main_container = st.container()
    sub_container = st.container()
    
    main_container.header(book)
    main_container.subheader("Author: {}".format(author))
    main_container.write(desc)
    
    col1.metric("Rating", rating)
    col2.metric("Voters", voters)
    col3.metric("Price", price)
    col4.metric("Page Count", page_count)

    sub_container.write("Publisher: {}".format(publisher))
    sub_container.write("Published Date: {}".format(year))

    if genre != "none":
        sub_container.write("Genres: ")
        for g in list(genre.split(",")):
            sub_container.caption("{}".format(g))

if page == 'Authors':
    book_names = (conn.query('SELECT author FROM books'))
    st.write("select the name of the author")
    author_name = st.selectbox('Author',book_names)

    book = conn.query(f"SELECT title FROM books WHERE author = '{author_name}';").values[0][0]
    desc = conn.query(f"SELECT description FROM books WHERE author = '{author_name}' limit 1;").values[0][0]
    author = conn.query(f"SELECT author FROM books WHERE author = '{author_name}' limit 1;").values[0][0]
    rating = conn.query(f"SELECT rating FROM books WHERE author = '{author_name}' limit 1;").values[0][0]
    voters = conn.query(f"SELECT voters FROM books WHERE author = '{author_name}' limit 1;").values[0][0]
    price = conn.query(f"SELECT price FROM books WHERE author = '{author_name}' limit 1;").values[0][0]
    publisher = conn.query(f"SELECT publisher FROM books WHERE author = '{author_name}' limit 1;").values[0][0]
    page_count  = conn.query(f"SELECT page_count FROM books WHERE author = '{author_name}' limit 1;").values[0][0]
    year = conn.query(f"SELECT published_date FROM books WHERE author = '{author_name}' limit 1;").values[0][0]
    genre = conn.query(f"SELECT genres FROM books WHERE author = '{author_name}' limit 1;").values[0][0]

    col1, col2, col3, col4 = st.columns(4)
    main_container = st.container()
    sub_container = st.container()
    
    main_container.header(book)
    main_container.subheader("Author: {}".format(author))
    main_container.write(desc)
    
    col1.metric("Rating", rating)
    col2.metric("Voters", voters)
    col3.metric("Price", price)
    col4.metric("Page Count", page_count)

    sub_container.write("Publisher: {}".format(publisher))
    sub_container.write("Published Date: {}".format(year))

    if genre != "none":
        sub_container.write("Genres: ")
        for g in list(genre.split(",")):
            sub_container.caption("{}".format(g))

if page == 'Range':
    st.title("Select your choice!")
    rating = st.slider("Select the rating range", 0.0, 5.0)
    price = st.slider("Select the price range", 0.0, 886.08)
    page_count = st.slider("Select the page count range", 0, 4544)

    books = conn.query('SELECT * FROM books where rating  >= {} and price >= {} and page_count >= {}'.format(rating,price,page_count))
    count = books.shape[0]
    st.write("Number of books found: {}".format(count))
    st.dataframe(books)




