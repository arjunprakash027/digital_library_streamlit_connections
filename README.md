# Welcome to the Digital Library!

This website allows you to search for books by title, author, or genre. You can also view detailed information about a book, including its rating, number of voters, price, page count, publisher, and published date.

To get started, select the page you want to view from the sidebar. The main content area will then display the corresponding information.

## Pages

The Digital Library has four pages:

* **All:** This page displays a list of all the books in the library. Users can sort the list by title, author, rating, or price.

* **Books:** This page allows users to search for books by title, author, or genre. Once a book is selected, the user can view detailed information about the book, including its rating, number of voters, price, page count, publisher, and published date.

* **Authors:** This page displays a list of all the authors in the library. Users can sort the list by name or number of books. Once an author is selected, the user can view a list of all the books by that author.

* **Range:** This page allows users to search for books by rating, price, and page count. Users can use the sliders to specify the range of values they want to search for. Once the search is complete, the user will see a list of books that meet the criteria.

## Connector Hackathon Submission
                
### SQLiteConnection Class

This code defines a `SQLiteConnection` class that can be used to connect to a SQLite database. The class inherits from the `ExperimentalBaseConnection` class, which provides a way to connect to a data source in Streamlit.

The `SQLiteConnection` class has a constructor that takes the URL of the SQLite database as a parameter. The class also has a `_connect()` method that returns a cursor object that can be used to execute queries against the database.

The `SQLiteConnection` class also has a `query()` method that takes a query as a parameter and returns a Pandas DataFrame with the results of the query. The `query()` method uses the `st.cache_data()` decorator to cache the results of the query for 3600 seconds.

## How to use the code

To use the `SQLiteConnection` class, you first need to create an instance of the class. You can do this by passing the URL of the SQLite database to the constructor. For example:

```python
conn = st.experimental_connection('db',type=SQLiteConnection,url = "db location")


Once you have created an instance of the class, you can use the `_connect()` method to get a cursor object. You can then use the cursor object to execute queries against the database. For example:

python
cursor = connection._connect()
cursor.execute('SELECT * FROM books')
results = cursor.fetchall()
```

We hope you enjoy using the website!

